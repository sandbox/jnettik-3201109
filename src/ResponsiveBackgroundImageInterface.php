<?php

namespace Drupal\prototype_backgrounds;

use Drupal\media\MediaInterface;

/**
 * Interface for BackgroundImage.
 */
interface ResponsiveBackgroundImageInterface {

  /**
   * Renders the CSS for responsive background images.
   *
   * @param string $selector
   *   The CSS selector to use.
   * @param \Drupal\media\MediaInterface $media
   *   The Media or File entity to use for the background.
   * @param string $responsive_image_style
   *   The ResponsiveImageStyle to use for the image.
   *
   * @return array
   *   Render array to be attached to site `head`.
   */
  public function getResponsiveStyles(string $selector, MediaInterface $media, string $responsive_image_style);

}
