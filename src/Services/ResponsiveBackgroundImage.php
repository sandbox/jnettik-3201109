<?php

namespace Drupal\prototype_backgrounds\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Uuid\Php;
use Drupal\media\MediaInterface;
use Drupal\prototype_backgrounds\ResponsiveBackgroundImageInterface;

/**
 * Class ResponsiveBackgroundImage.
 */
class ResponsiveBackgroundImage extends BackgroundImage implements ResponsiveBackgroundImageInterface {

  /**
   * {@inheritdoc}
   */
  public function getResponsiveStyles(string $selector, MediaInterface $entity, string $responsive_image_style) {
    // @todo add Responsive Image support.
    return [];
  }

}
