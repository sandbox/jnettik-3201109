<?php

namespace Drupal\prototype_backgrounds;

use Drupal\media\MediaInterface;

/**
 * Interface for BackgroundImage.
 */
interface BackgroundImageInterface {

  /**
   * Renders the CSS for the background image.
   *
   * @param string $selector
   *   The CSS selector to use.
   * @param \Drupal\media\MediaInterface $media
   *   The Media or File entity to use for the background.
   * @param string $image_style
   *   The ImageStyle to use for the image.
   *
   * @return array
   *   Render array to be attached to site `head`.
   */
  public function getStyles(string $selector, MediaInterface $media, string $image_style);

}
